Role Name
=========

Linux pam users (ssh)
Requirements
------------

Add ssh public key to /files/ssh_keys directory

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:
```
- hosts: host
  roles:
    - users
```
License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a
website (HTML is not allowed).
