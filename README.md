# elk-ansible-docker



## Getting started

Предварительно подговить вируальную машину, заранее обновив до нужной версии утилиты (Python, pip, docker)

В нашем случае, нужно создать субдомен и привязать его на наш сервер.

Добавить этот сервер в инвентори, и создать файл в playbooks/setup/elk.yml с указанным блоком серверов(В случае нескольких нод)

Важно иметь доступ до сервера с ссш ключами

Запустить плэйбук 

## Для того что бы не ходить в вм от рута, этот роль позволяет создать пользователя.
## В директории ssh_keys/$USERNAME.pub указать свой публичный ключ, создать пароль с  командой "mkpasswd --method=sha-512" и заменить в переменных group_vars/all 
$ ansible-playbook -i inventory/prod.ini playbooks/setup/users.yml -bK -u $USERNAME

## проверить правильность версии докера
$ ansible-playbook -i inventory/prod.ini playbooks/setup/docker.yml -bK -u $USERNAME 
## Убедитесь что установленны python зависимости докера
$ ansible-playbook -i inventory/prod.ini playbooks/setup/elk.yml -bK -u $USERNAME
## В директории roles/nginx/tasks/main.yml вы можете поменять авторизациюнные данные для доступа к дашборду кибаны
$ ansible-playbook -i inventory/prod.ini playbooks/setup/nginx.yml -bK -u $USERNAME  # 

